import cv2
imageClassifier="haarcascade_frontalface_default.xml"
smileDetection="haarcascade_smile.xml"
video=cv2.VideoCapture(0)
#image="imageContainFolder/Elon-Musk.jpg"
classifier=cv2.CascadeClassifier(imageClassifier)
smileClassifier=cv2.CascadeClassifier(smileDetection)
while True:
    successFul,frame=video.read()
    greyishImage=cv2.cvtColor(frame,cv2.COLOR_BGR2GRAY)
    faceCoordinate= classifier.detectMultiScale(greyishImage)
    for i in range(len(faceCoordinate)):#This returns whole face
        x,y,h,w=faceCoordinate[i]
        drawRectangle=cv2.rectangle(frame,(x,y),(x+h,y+w),(1,111,1),4)
        #We can find only smile in face instead of whole image
        theFace=frame[y:y+w,x:x+h]#Find the smile in small images instead of whole frame
        #theFace=(x,y,h,w)

        #Draw rectangle while smilin in the face
        smileinGrey=cv2.cvtColor(theFace,cv2.COLOR_BGR2GRAY)
        smiles= smileClassifier.detectMultiScale(smileinGrey,scaleFactor=1.7,minNeighbors=20)
        for x_,y_,w_,h_ in smiles:
            #smileRectangle=cv2.rectangle(theFace,(x_,y_),(x_+w_,y_+h_),(255,0,0),4)
            #Instead of drawing the rectangle in face just tag the smile

            if len(smiles)>0:
                cv2.putText(frame,"Smiling",(x,y+h+60),fontFace=cv2.FONT_HERSHEY_COMPLEX,color=(255,255,255),fontScale=2)

    cv2.imshow("Elon musk",frame)
    key=cv2.waitKey(1)
    if key==81 or key==113:
        break
